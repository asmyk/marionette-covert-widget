# Zadanie rekrutacyjne na widget do konwersji liczb

Wykorzystałem tutaj prosty starter do Marionette/Backbone na ES6. Sam widget zrobiłem jako komponent który można wykorzystać w wielu miejscach w projekcie.

Widget składa się z trzech inputów. Każdy input ma w sobie prostą walidację. Inputy do wprowadzenia systemu liczbowego przymują wartosci pomiędzy 2 a 36, a input sprawdza czy ma wartość o długości > 1.

Przykładowe wykorzystanie:
```javascript
 let conventerView = new ConventerView({
            model: new ConventerModel({
                sourceSystem: 10,
                destSystem: 7
            })
        });
 this.showChildView('conventerRegion', conventerView); // wyswietlenie widgetu
 let convertedValue = conventerView.getConvertedNumber(); // wartosc po konwersjii
```

Metoda getConvertedNumber() zwróci wartość po konwersjii, albo null w przypadku nieprawidłowej wartości liczby do przekonwertowania.

Przykładowe użycie jest w pliku IndexView.js

Myślę, że taki kawałek kodu wystarczy na zadanie testowe. Widać tutaj architekture, styl pisania etc. Można by dołozyć translację, zmodyfikować style i rozbudować zachowanie inputów. Nie robiłem tego, bo widget miał być prosty :) 

## Opdalenie projektu

#### 1. Install gulp globally:

```sh
$ npm install --global gulp
```

#### 2. Install project dependencies:

```sh
$ npm install
```

#### 3. Run gulp

Start a local dev environment:
```sh
$ gulp
```