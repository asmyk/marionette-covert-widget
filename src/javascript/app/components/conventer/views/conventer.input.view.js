import Marionette from 'backbone.marionette';
import template from './../templates/conventer.input';


let InputView = Marionette.View.extend({
    template,
    templateContext() {
        return { label: this.getOption("label") || "Liczba" }
    },
    ui: {
        input: '.js-input',
        inputAlert: ".js-input-alert"
    },
    events: {
        'change @ui.input': 'onInputChange'
    },
    onInputChange(e) {
        let value = e.target.value;
        this.model.set("value", value);

        if (this.model.isValid()) {
            this.ui.inputAlert.toggleClass("hide-text", true);
            this.trigger(InputView.EVENTS.INPUT_CHANGED, value);
        } else {
            this.ui.inputAlert.toggleClass("hide-text", false);
            this.trigger(InputView.EVENTS.INPUT_CHANGED, null);
        }
    }
});

InputView.EVENTS = {
    INPUT_CHANGED: "input:changed"
};

export default InputView;