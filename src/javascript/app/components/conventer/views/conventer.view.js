import Marionette from 'backbone.marionette';
import _ from "underscore";
import template from './../templates/conventer';
import InputView from "./conventer.input.view";
import InputModel from "./../models/conventer.input.model";
import SystemInputModel from "./../models/conventer.system-input.model";


export default Marionette.View.extend({
    className: 'conventer',
    template,
    regions: {
        sourceSystemRegion: ".js-src-system-input-region",
        destSystemRegion: ".js-dest-system-input-region",
        numberInputRegion: ".js-number-input-region"
    },

    onRender() {
        let sourceSystemInputView = new InputView({ label: "Źródłowy system liczbowy", model: new SystemInputModel({ value: this.model.get("sourceSystem") }) }),
            destSystemInputView = new InputView({ label: "Docelowy system liczbowy", model: new SystemInputModel({ value: this.model.get("destSystem") }) }),
            numerInputView = new InputView({ label: "Liczba do przekonwertowania", model: new InputModel({ value: this.model.get("number") }) });

        this.showChildView("sourceSystemRegion", sourceSystemInputView);
        this.showChildView("destSystemRegion", destSystemInputView);
        this.showChildView("numberInputRegion", numerInputView);

        this.listenTo(sourceSystemInputView, InputView.EVENTS.INPUT_CHANGED, function (value) {
            this.model.set("sourceSystem", +value);
        });

        this.listenTo(destSystemInputView, InputView.EVENTS.INPUT_CHANGED, function (value) {
            this.model.set("destSystem", +value);
        });

        this.listenTo(numerInputView, InputView.EVENTS.INPUT_CHANGED, function (value) {
            this.model.set("number", value);
        });
    },

    getConvertedNumber: function () {
        let convertedValue = "",
            num = parseInt(this.model.get("number"), this.model.get("sourceSystem"));

        convertedValue = num.toString(this.model.get("destSystem"));

        return (this.model.isValid() && !_.isNaN(num)) ? convertedValue : null;
    }
});
