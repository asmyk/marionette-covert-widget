import Backbone from 'backbone';
import _ from 'underscore';
import InputModel from "./conventer.input.model"

let ConventerInputModel = InputModel.extend({
    isValid() {
        let currentValue = this.get("value");
        return currentValue >= 2 && currentValue <= 36
    }
});

export default ConventerInputModel;
