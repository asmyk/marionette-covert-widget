import Backbone from 'backbone';
import _ from 'underscore';

export default Backbone.Model.extend({
    defaults() {
        return {
            value: ""
        }
    },
    isValid() {
        return !_.isEmpty(this.get("value"));
    }
});

