import Backbone from 'backbone';
import _ from "underscore";

let ConventerModel = Backbone.Model.extend({
    defaults() {
        return {
            sourceSystem: 0,
            destSystem: 0,
            number: "1500"
        }
    },
    isValid() {
        return _.every(this.pick(["sourceSystem", "destSystem"]), _.isNumber) && !_.isEmpty(this.get("number"));
    }
});

export default ConventerModel;
