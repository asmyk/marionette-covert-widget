import BaseView from './BaseView';
import template from 'templates/index.hbs';
import ConventerView from '../components/conventer/views/conventer.view';
import ConventerModel from '../components/conventer/models/conventer.model';

export default BaseView.extend({

    className: 'page page-index',

    template,

    ui: {
        "convertBtn": ".js-convert-btn",
        "convertResult": ".js-convert-result"
    },

    regions: {
        conventerRegion: '.js-conventer-region'
    },

    events: {
        "click @ui.convertBtn": "onConvertBtnClick"
    },

    onRender() {
        let conventerView = new ConventerView({
            model: new ConventerModel({
                sourceSystem: 10,
                destSystem: 7
            })
        });

        this.showChildView('conventerRegion', conventerView);
    },

    onConvertBtnClick() {
        let convertedValue = this.getChildView('conventerRegion').getConvertedNumber();

        (convertedValue) ? this.ui.convertResult.html(convertedValue) : this.ui.convertResult.html("Błąd w obliczeniach. Sprawdz czy wpisałeś poprawną liczbę dla wybranego systemu liczbowego.");
    }
});
