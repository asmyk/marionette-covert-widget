import Marionette from 'backbone.marionette';
import template from 'templates/app-layout';
import attachFastClick from 'fastclick';
import $ from 'jquery';
import { LOADING_CLASS } from 'utils/constants';

export default Marionette.View.extend({

    className: 'is-loading',

    template,

    regions: {
        main: '#main',
    },

});
